import { NotImageDirective } from './not-image.directive';
import {  ElementRef } from '@angular/core';
describe('NotImageDirective', () => {
  it('should create an instance', () => {
    let elementImg: ElementRef;
    const directive = new NotImageDirective(elementImg);
    expect(directive).toBeTruthy();
  });
  it('call onError', () => {
    let elementImg: ElementRef = {nativeElement: {src: ""}};
    const directive = new NotImageDirective(elementImg);
    directive.onError();
    expect(elementImg.nativeElement.src).toEqual("../../assets/images/image-not-found.png");
  });

});
