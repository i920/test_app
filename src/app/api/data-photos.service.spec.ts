import { TestBed } from '@angular/core/testing';

import { DataPhotosService } from './data-photos.service';

describe('DataPhotosService', () => {
  let service: DataPhotosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataPhotosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getPhotos called', () => {
    const photos = service.getPhotos();
    expect(photos.length).toEqual(4000);
    photos.forEach(photo => {
      expect(photo.id).toBeDefined();
      expect(photo.photo).toBeDefined();
      expect(photo.text).toBeDefined();
    });
  });

});
