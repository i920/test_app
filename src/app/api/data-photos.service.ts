import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataPhotosService {

  private API_URL = environment.API_URL;
  private totalItems = 4000;

  getPhotos(){
    return this.generateDataPhotos();
  }

  private generateDataPhotos(){
    const photos = [];
    const listNums = Array.from(Array(this.totalItems).keys());
    for (let i = 1; i <= this.totalItems; i++) {
      const randomIndex= Math.floor(Math.random() * (this.totalItems - i));
      const itemPhoto = this.createItem(listNums.splice(randomIndex, 1)[0]);
      photos.push(itemPhoto);
    }
    return photos;
  }

  private createItem(id: number){
    return {
    	id,
    	photo: this.API_URL.replace('{ID}', id.toString()),
    	text: this.generateRandomString()
    };
  }

  private generateRandomString(){
    const totalCharacters = 10;
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for ( let i = 0; i < totalCharacters; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

}

