export interface DataPhotos {
    id: string;
    photo: string;
    text: string;
}
