
import { Component, ViewChild, OnInit} from '@angular/core';
import { IonContent, IonInfiniteScroll } from '@ionic/angular';
import { DataPhotosService } from '../api/data-photos.service';
import { DataPhotos } from './models/DataPhotos';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;

  itemPhotos = [];
  private activeLang = 'es';
  private dataPhotos: DataPhotos[];
  private searchPhotos = [];
  private numItemsForPage = 10;
  private timeToScroll = 500;
  private durationLoadingScroll = 500;

  constructor(private dataPhotosService: DataPhotosService, private translate: TranslateService) {
    this.translate.setDefaultLang(this.activeLang);
    this.translate.use(this.activeLang);
  }

  ngOnInit(){
    this.dataPhotos = this.dataPhotosService.getPhotos();
    this.searchPhotos = [...this.dataPhotos];
    this.appendItems();
  }

  loadData(event: any): void {

    setTimeout(() => {
      event.target.complete();
      this.appendItems();
      if (this.itemPhotos.length >= this.searchPhotos.length) {
        event.target.disabled = true;
      }
    }, this.durationLoadingScroll);
  }

  searchAction(text: any): void {
    this.itemPhotos = [];
    const val = text.target.value;
    this.searchPhotos = this.dataPhotos.filter(item => {
      const itemText = item.text.toLocaleLowerCase();
      const itemId = item.id.toString();
      const isValContainedInText = itemText.includes(val.toLocaleLowerCase());
      const isValContainedInId = itemId.includes(val.toLocaleLowerCase());
      return isValContainedInText || isValContainedInId;
    });
    this.appendItems();
  }

  scrollToTop(): void{
    this.content.scrollToTop(this.timeToScroll);
  }

  private appendItems(itemsToAdd: number = this.numItemsForPage): void{
    const amountItemsAdded = this.itemPhotos.length;
    const newItems = amountItemsAdded + itemsToAdd;
    for(let i = amountItemsAdded; i < newItems && i < this.searchPhotos.length; i++){
      this.itemPhotos.push(this.searchPhotos[i]);
    }
  }

}
