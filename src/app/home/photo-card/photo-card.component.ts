import { Component, Input } from '@angular/core';
import { DataPhotos } from '../models/DataPhotos';

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.scss'],
})
export class PhotoCardComponent{
  @Input() data: DataPhotos[] = []; // decorate the property with @Input()

  trackByFn(index: number, item: DataPhotos) {
    return item.id;
  }
}
