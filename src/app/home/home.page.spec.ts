import { ComponentFixture, TestBed, waitForAsync, getTestBed, tick } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HomePage } from './home.page';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { DataPhotosService } from '../api/data-photos.service';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let dataPhotosService: DataPhotosService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot(),
        TranslateTestingModule.withTranslations({})
      ],
      providers: [
        { provide: DataPhotosService, useClass: DataPhotosService }
      ],

    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    dataPhotosService = TestBed.inject(DataPhotosService);
    component = fixture.componentInstance;

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
    fixture.detectChanges();
  });

  describe('call functions', () => {
    const photosMock = require('./mocks/photos-mock.json');

    it('ngOnInit called', () => {
      component.ngOnInit();
      expect(component.itemPhotos.length).toEqual(10);
    });

    it('loadData called',()=> {
      jasmine.clock().install();
      const event = { target:{ complete:jasmine.createSpy('complete') } };
      spyOn(dataPhotosService, 'getPhotos').and.returnValue(photosMock);
      component.ngOnInit();
      component.loadData(event);
      jasmine.clock().tick(600);
      const loadedDataLength = (photosMock.length > 20) ? 20 : photosMock.length;
      expect(component.itemPhotos.length).toEqual(loadedDataLength);
      jasmine.clock().uninstall();
    });


    it('searchAction called', () => {
      const text = { target: {value : "1"}};
      spyOn(dataPhotosService, 'getPhotos').and.returnValue(photosMock);
      component.ngOnInit();
      component.searchAction(text);
      expect(component.itemPhotos.length).toEqual(4);
    });

  });

});



